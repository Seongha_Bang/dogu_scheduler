# Dogu Scheduler


ROS에서 예약 작업을 할 수 있도록 도와주는 노드


## 필요한 모듈

설치는 아래 **사용-준비** 문단에서 설명한다.

- apscheduler: 스케줄링 모듈
- pytz: UTC 모듈


## 파일 준비


원하는 디렉터리에 JSON 파일 생성

```
{
	"m": "*",
	"h": "*",
	"dom": "*",
	"mon": "*",
	"dow": "*",
	"utc": "Asia/Seoul",
	"schedule": "schedule",
	"task": "task",
	"map": "Map",
	"path": "Path"
}
```

- 리눅스 Cron과 동일한 부분
 - **M**inute: 원하는 시각의 분 (0 ~ 59)
 - **H**our: 원하는 시각의 시 (0 ~ 23)
 - **D**ay **O**f **M**onth: 원하는 날짜의 일 (1 ~ 31)
 - **MON**th: 원하는 날짜의 달 (1 ~ 12)
 - **D**ay **O**f **W**eek: 원하는 요일 (0: 일 1: 월, ... , 6: 토, 7: 일)

- 그 외
 - utc: 위에서 지정한 시각의 UTC
 - schedule: 스케줄 ID
 - task: 작업 ID
 - map: 지도 파일 위치
 - path: 경로 파일 위치


## Library packages installation


```
bash setup/install.sh
```


## Catkin make


```
cd ~/catkin_ws
catkin_make_isolated --source src/dogu_scheduler
```


## Auto launch setting


```
cd dogu_scheduler
bash setup/autorun.sh
```

### 실행


start.launch 파일로 실행

- 파라미터
 - work_dir: JSON 파일을 준비했던 디렉터리 (변수 - directory)
 - json: JSON 파일 이름 (변수 - json_name)


## 메시지 형태


- ScheduleInfo.msg (위의 JSON 파일과 동일)
  - string m
  - string h
  - string dom
  - string mon
  - string dow
  - string utc
  - string schedule
  - string task
  - string map
  - string path


- ScheduleMsg.msg
  - string schedule: 스케줄 ID
  - string task: 작업 ID
  - string map: 지도 파일 위치
  - string path: 경로 파일 위치


- ConfSig.srv
  - 요청
    - bool stop: 스케줄러 종료 여부
    - bool start: 스케줄러 시작 여부
  - 응답
    - (없음)


- CronSig.srv
  - 요청
    - string schedule: 스케줄 ID
    - string task: 작업 ID
    - string map: 지도 파일 위치
    - string path: 경로 파일 위치
  - 응답
    - (없음)


- ScriptFeedback.srv (위의 JSON 파일과 동일)
  - 요청
    - string m
    - string h
    - string dom
    - string mon
    - string dow
    - string utc
    - string schedule
    - string task
    - string map
    - string path
  - 응답
    - (없음)


## 토픽 설명


- scheduler_status
  - ScheduleInfo.msg 사용
  - 현재 활성화된 스케줄(JSON)의 내용 발행
  - 스케줄러 비활성화 시 빈 토픽 발행


- robot_schedule\_trigger
  - ScheduleMsg.msg 사용
  - 평소엔 빈 문자열 발행
  - 예약된 시간이 되면 Map과 Path 값 발행
  - 동일한 /robot_schedule\_feedback 토픽 수신 시 다시 빈 문자열 발행


- robot_schedule\_feedback
  - ScheduleMsg.msg 사용
  - /robot_schedule\_trigger 토픽을 수신한 쪽에서 마지막으로 수신한 값 발행
  - /robot_schedule\_trigger 토픽 수신 여부를 스케줄링 노드에게 알려주기 위해 사용


## 서비스 설명


- schedule_config\_server
  - ConfSig.srv 사용
  - 스케줄러를 종료, 시작시킬 수 있는 관리 서비스
  - 종료와 시작이 모두 참일 경우엔 재시작


- schedule_detect\_server
  - CronSig.srv 사용
  - 예약한 시간을 감지하는 서버
  - Scheduler.py 스크립트를 생성, 실행해 감지


- schedule_script\_feedback
  - ScriptFeedback.srv 사용
  - Sceduler.py 실행시 JSON에 설정된 데이터 전달 위해 사용


## 사용


### 준비


필요한 모듈을 설치하기 위해 setup/setup.sh을 실행한다.


### 실행


start.launch 파일을 통해 실행한다.

launch 파일 내의 directory 변수로 work_dir 파라미터,   
json_name 변수로 json 파라미터를 설정할 수 있다.   

미리 준비한 파일대로 설정해 실행하면 된다.


### Topic - /scheduler_status


실행하면 JSON 파일에 설정된 대로 스케줄러가 작동된다.   
/scheduler_status 토픽을 확인해보면 현재 어떤 스케줄이 실행되어 있는지 알 수 있다.


### Topic - /robot_schedule\_trigger


잘 실행되었다면 예약된 시간에 /robot_schedule\_trigger 토픽으로   
JSON에 설정해뒀던 Map과 Path 파일의 경로가 발행된다.   
예약된 시간이 되기 전에는 빈 값을 발행하다가   
시간이 지난 뒤부터 잘 수신되었는지 대답하기 전까지는   
계속 schedule, task, Map, Path 정보가 발행된다.


### Topic - /robot_schedule\_feedback


/robot_schedule\_trigger 토픽을 수신한 뒤,
/robot_schedule\_feedback 토픽을 이용해서 수신 여부를 대답할 수 있다.   
수신된 schedule, task, Map, Path 정보를   
그대로 /robot_schedule\_feedback 토픽에 담아 발행하면 된다.   
스케줄러에서 데이터를 비교해 일치하면 /robot_schedule\_trigger 토픽에는 다시 빈 값이 발행된다.


### Service - /schedule_config\_server


원한다면 스케줄 작업을 잠시 멈출 수도 있다.   
/schedule_config\_server 서비스를 이용해 스케줄 시작 및 중지가 가능하다.

stop에 true, start에 false로 서비스를 요청하면 스케줄러가 멈춘다.   
반대로 다시 실행시키고 싶을 때에는 stop에 false, start에 true를 주면 된다.

만약 스케줄러 실행중에 JSON파일이 변경된다면 스케줄러를 다시 시작해야 한다.      
스케줄러는 시작됐을 때의 데이터를 기준으로 작동되기 때문이다.   
이럴 때는 stop과 start에 모두 true를 주면 종료후 다시 시작된다.

/schedule_config\_server 서버에 서비스를 요청하면   
응답할 때 시작 및 중지의 성공 여부를 반환한다.   
1이면 성공, 0이면 실패를 의미한다.
