#!/usr/bin/env python

import rospy
from dogu_scheduler import *
from dogu_scheduler.msg import *
from dogu_scheduler.srv import *
from os import system, popen, pipe
import signal

# Scheduler.py console window active status
SCHEDULER_CONSOLE = False


# Scheduler class
class Scheduler:
    def __init__(self):
        # Create Scheduler.py
        with open(work_dir + 'Scheduler.py', 'w') as f:
            f.write(FileData.scheduler())

        system('sudo chmod 755 {}Scheduler.py'.format(work_dir))

        # Initialization
        self.script_data = ScheduleInfo()
        self.trigger_data = ScheduleMsg()
        self.feedback_data = ScheduleMsg()

    # Detect schedule time from Scheduler.py
    def detect(self, req):
        # Print and publish data (map and path file directory)
        print(
            '[Receive] schedule: {} / task: {} / map: {} / path: {}'.format(req.schedule, req.task, req.map, req.path))
        self.trigger_data = ScheduleMsg(req.schedule, req.task, req.map, req.path)

        return CronSigResponse()

    # Control Scheduler.py
    def config(self, req):
        success = True

        # Stop Scheduler.py
        if req.stop:
            system('pkill -9 -f Scheduler.py')
            self.trigger_data = ScheduleMsg()
            rospy.sleep(0.5)
            success = not self.get_script_status()

        # Start Scheduler.py
        if req.start:
            if SCHEDULER_CONSOLE:
                # Active console window
                cmd = 'gnome-terminal -e "python {}Scheduler.py {}{}"'.format(work_dir, work_dir, json)
            else:
                # Disable console window
                cmd = 'python {}Scheduler.py {}{} >/dev/null 2>&1 &'.format(work_dir, work_dir, json)

            # Start
            system(cmd)
            rospy.sleep(0.5)
            success = self.get_script_status()

        # Specify results based on success
        if success:
            result = 1
        else:
            result = 0

        return ConfSigResponse(result)

    # Is Scheduler.py operating?
    def get_script_status(self):
        return popen('ps -ef | grep -v grep | grep Scheduler.py').read() != ''

    # Schedule data publish
    def script_feedback(self, req):
        self.script_data = ScheduleInfo(req.m, req.h, req.dom, req.mon, req.dow, req.utc, req.schedule, req.task,
                                        req.map, req.path)
        return ScriptFeedbackResponse()

    # Get trigger feedback data
    def trigger_feedback(self, req):
        self.feedback_data = ScheduleMsg(req.schedule, req.task, req.map, req.path)


# Start node
def dogu_scheduler():
    # Initialization node
    rospy.init_node('scheduling_node')

    # Initialization publisher
    scheduler_status = rospy.Publisher('scheduler_status', ScheduleInfo, queue_size=10)
    schedule_trigger = rospy.Publisher('robot_schedule_trigger', ScheduleMsg, queue_size=10)

    # Initialization service
    s = Scheduler()

    service_chck = rospy.Service('schedule_script_feedback', ScriptFeedback, s.script_feedback)
    service_play = rospy.Service('schedule_detect_server', CronSig, s.detect)
    service_conf = rospy.Service('schedule_config_server', ConfSig, s.config)

    # Initialization subscriber
    rospy.Subscriber("robot_schedule_feedback", ScheduleMsg, s.trigger_feedback)

    # Start Scheduler.py
    sig = ConfSig()
    sig.stop = False
    sig.start = True
    s.config(sig)

    # Loop
    loop = rospy.Rate(1)

    while not rospy.is_shutdown():
        scheduler_status.publish(s.script_data)
        schedule_trigger.publish(s.trigger_data)

        if not s.get_script_status():
            s.script_data = ScheduleInfo()

        if s.trigger_data.schedule == s.feedback_data.schedule != '' and s.trigger_data.task == s.feedback_data.task != '' and s.trigger_data.map == s.feedback_data.map != '' and s.trigger_data.path == s.feedback_data.path != '':
            s.trigger_data = ScheduleMsg()
            s.feedback_data = ScheduleMsg()

        loop.sleep()

    # Stop Scheduler.py when node stopped
    sig = ConfSig()
    sig.stop = True
    sig.start = False
    s.config(sig)


if __name__ == '__main__':
    # For use pipe character - |
    signal.signal(signal.SIGPIPE, signal.SIG_DFL)

    # Print required modules
    print('\n[Modules required: apscheduler, pytz]\n')

    try:
        # Get parameters
        work_dir = rospy.get_param('work_dir')
        json = rospy.get_param('json')

        # Start node
        dogu_scheduler()
    except rospy.ROSInterruptException:
        pass
