echo "[Setup Dogu Scheduler Service]"

# Copy script file
echo "[Copy start script file]"
bash -c "sudo cp services/scheduling-start.sh /home/"
echo "[Finish copy script file]"

# Copy service script file
echo "[Copy service file]"
bash -c "sudo cp services/dogu-scheduling.service /etc/systemd/system/"
echo "[Finish copy service file]"

# Enable service
bash -c "sudo systemctl enable dogu-scheduling.service"

bash -c "sudo systemctl start dogu-scheduling.service"
echo "[Finish setup]"